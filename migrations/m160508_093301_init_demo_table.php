<?php

use yii\db\Migration;

class m160508_093301_init_demo_table extends Migration
{
    public function up()
    {
		$this->createTable(
			'students',
			[
				'id'=> 'pk',
				'name'=> 'string',
				'date'=>'date',
				'notes'=> 'text',
			],
			'ENGINE=InnoDB'
		);
    }

    public function down()
    {
        echo "m160508_093301_init_demo_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
